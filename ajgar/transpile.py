# transpile.py
import re
import logging

module_logger = logging.getLogger(__name__)

builtin_func_mapping = {
    "अब्स": "abs",
    "दैलअट्र्": "delattr",
    "हाश": "hash",
    "मैमोरीवीउ": "memoryview",
    "सैट": "set",
    "आल": "all",
    "डिक्ट": "dict",
    "हैल्प": "help",
    "मिन": "min",
    "सैटअट्र": "setattr",
    "एनी": "any",
    "डिर": "dir",
    "एक्स": "hex",
    "नैक्स्ट": "next",
    "स्लय्स": "slice",
    "आस्की": "ascii",
    "डिवमोड": "divmod",
    "आयडी": "id",
    "अबजॆक्ट": "object",
    "सोर्टॆड": "sorted",
    "बिन": "bin",
    "एनुमैरेट": "enumerate",
    "इनपुट": "input",
    "अक्ट": "oct",
    "स्टाटिकमैथड": "staticmethod",
    "बूल": "bool",
    "एवाल": "eval",
    "इन्ट": "int",
    "ओपैन": "open",
    "स्ट्र": "str",
    "ब्रैकपोय्न्ट": "breakpoint",
    "एज़ैक": "exec",
    "इज़इन्स्टन्स": "isinstance",
    "ओर्ड": "ord",
    "सम": "sum",
    "बय्टअरेय": "bytearray",
    "फ़िल्ट्र": "filter",
    "इज़सबक्लास": "issubclass",
    "पौव": "pow",
    "सुप्र": "super",
    "बय्ट्स": "bytes",
    "फ़्लोट": "float",
    "इट्र": "iter",
    "प्रिन्ट": "print",
    "टुप्ल": "tuple",
    "कालब्ल": "callable",
    "फ़ोरमाट": "format",
    "लैन": "len",
    "प्रपैर्टी": "property",
    "टय्प": "type",
    "च्र": "chr",
    "फ़्रोज़ैनसैट": "frozenset",
    "लिस्ट": "list",
    "रय्न्ज": "range",
    "वार्ज़": "vars",
    "क्लासमैथड": "classmethod",
    "गैटअट्र": "getattr",
    "लोकल्स": "locals",
    "रैप्र्": "repr",
    "ज़िप": "zip",
    "कमपय्ल": "compile",
    "ग्लोबल्स": "globals",
    "माप": "map",
    "रैवैर्स्ड": "reversed",
    "__इम्पोर्ट__": "__import__",
    "चमप्लैक्स": "complex",
    "हासअट्र": "hasattr",
    "माक्स": "max",
    "राउन्ड": "round"
}

reserved_keyword_mapping = {
    "फल्स": "False",
    "अवय्ट": "await",
    "एल्स": "else",
    "इम्पोर्ट": "import",
    "पास": "pass",
    "नन": "None",
    "ब्रय्क": "break",
    "एक्सैप्ट": "except",
    "इन": "in",
    "रेज़": "raise",
    "ट्रू": "True",
    "क्लास": "class",
    "फ़यनली": "finally",
    "इज़": "is",
    "रीटुर्न": "return",
    "आन्ड": "and",
    "क्नटिनयु": "continue",
    "फ़ोर": "for",
    "लम्ब्ड": "lambda",
    "ट्रय": "try",
    "अस": "as",
    "डैफ़": "def",
    "फ़्रम": "from",
    "ननलोकल": "nonlocal",
    "वय्ल": "while",
    "असैर्ट": "assert",
    "डैल": "del",
    "ग्लोबल": "global",
    "नट": "not",
    "विथ": "with",
    "एसिन्क": "async",
    "एलिफ़": "elif",
    "इफ़": "if",
    "ओर": "or",
    "यील्ड": "yield"
}

dunder_method_mapping = {
    "__इनिट__": "__init__"
}


def transpile(source_txt: str) -> str:
    """
    Transpile some ajgar source code to Python source code.
    """
    dest_txt = source_txt
    for name, value in builtin_func_mapping.items():
        dest_txt = dest_txt.replace(name, value)

    dest_txt_split = re.split(r"(\s)", dest_txt)

    for idx in range(len(dest_txt_split)):
        chunk = dest_txt_split[idx]
        if chunk.strip() in reserved_keyword_mapping:
            dest_txt_split[idx] = reserved_keyword_mapping[chunk.strip()]

    dest_txt = "".join(dest_txt_split)

    for name, value in dunder_method_mapping.items():
        dest_txt = dest_txt.replace(name, value)

    return dest_txt
