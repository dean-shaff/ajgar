import sys
import codecs
import runpy
import os

from . import transpile


def ajgar(source_file: str) -> None:
    """
    Run some agjar source code
    """
    with codecs.open(source_file, mode="r", encoding="utf-8") as fd:
        contents = fd.read()

    py_src = transpile.transpile(contents)
    with codecs.open("__ajgar.py", mode="w", encoding="utf-8") as fd:
        fd.write(py_src)
    runpy.run_module("__ajgar", run_name="__main__")
    os.remove("__ajgar.py")


if __name__ == "__main__":
    ajgar(sys.argv[1])
