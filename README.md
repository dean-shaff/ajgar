```python
class MyClass:

  def __init__(self, *args, **kwargs):
    self.attr = None

def main():
  print("main")
  obj = MyClass()
  repr(obj)
```


```
क्लास मईक्लास:

  दॆफ __इनिट__(सॆल्फ, *अर्ग्स, **क्वर्ग्स):
    सॆल्फ।आट्र् = नन

दॆफ मेन():
  प्रिन्ट("main")
  ओब्ज = मईक्लास()
  रॆप्र(ओब्ज)
```
