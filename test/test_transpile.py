import os
import unittest
import logging
import codecs

import ajgar.transpile

cur_dir = os.path.dirname(os.path.abspath(__file__))


class TestTranspile(unittest.TestCase):

    def test_transpile(self):
        test_file_path = os.path.join(cur_dir, "test_ajgar.ajgar")
        with codecs.open(test_file_path, mode="r", encoding="utf-8") as fd:
            contents = fd.read()

        py_src = ajgar.transpile.transpile(contents)
        with open("transpiled.py", mode="w") as fd:
            fd.write(py_src)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
